using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace nots_proxy
{
    public class tcpProxy
    {
        private readonly TcpListener _listener;
        public tcpProxy(IPAddress ipAddress, int port)
        {
            _listener = new TcpListener(ipAddress, port);
            _listener.Start();
        }

        public void listen()
        {
            while (true)
            {
                listenAsync();
            }
        }

        private async void listenAsync()
        {
            var client = await _listener.AcceptTcpClientAsync();
            var stream = client.GetStream();
            var running = true;

            var newClient = new TcpClient("http://osserver.net", 80);
//            var ip = IPAddress.Parse("http://osserver.net");

            var bufferSize = 1024; 
            var buffer = new byte [bufferSize];
            var i = newClient.Client.Receive(buffer);

            var resString = Encoding.UTF8.GetString(buffer, 0, i);
            Console.WriteLine(resString);
            var res = Encoding.UTF8.GetBytes(resString);
            
                
                
//            await newClient.ConnectAsync(ip, 80);
//            var serverStream = newClient.GetStream();
//            var msg = "";
//            Console.WriteLine(serverStream.DataAvailable);
//            if (serverStream.CanRead)
//            {
//                while (serverStream.DataAvailable)
//                {
//                    Console.WriteLine("enter while");
//                    
//                    var readByte = await serverStream.ReadAsync(buffer, 0, bufferSize);
//                    var newMsg = Encoding.UTF8.GetString(buffer, 0, readByte);
//                    msg += newMsg;
//                    Console.WriteLine(msg);
//                }
//            }
//            
//            Console.WriteLine(msg);

            await stream.WriteAsync(buffer);
            
            stream.Close();
            client.Close();

//            while (running)
//            {
//                byte[] buffer = new byte[client.ReceiveBufferSize];
//                var readBytes = await stream.ReadAsync(buffer, 0, client.ReceiveBufferSize);
//                string newMessage = Encoding.UTF8.GetString(buffer, 0, readBytes);
//                message += newMessage;
//
//                Console.WriteLine(message);
//            }
        }
    }
}